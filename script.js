const subnet = {
    data: {
        hostip: [0,0,0,0],
        subnet: [0,0,0,0],
        subnetmask: [],
        boardcast: [],
        maskbit: 1
    },
    methods: {
        numToBinary(n, order = 'start', length = 8) {
            if(order === 'start') return Number(n).toString(2).padStart(length,0);
            return Number(n).toString(2).padEnd(length,0);
        },
        lookupSubnetMask(bit) {
            const mysubnet = {
                bit_mask: 1,
                class: 'Invalid',
                subnet_mask: [255,255,255,255]
            }

            if(bit >= 0 && bit <= 127) {
                mysubnet.bit_mask = 8
                mysubnet.class = 'Class A Address'
                mysubnet.subnet_mask = [255,0,0,0]
            }
            else if(bit >= 128 && bit <= 191) {
                mysubnet.bit_mask = 16
                mysubnet.class = 'Class B Address'
                mysubnet.subnet_mask = [255,255,0,0]
            }
            else if(bit >= 192 && bit <= 223) {
                mysubnet.bit_mask = 24
                mysubnet.class = 'Class C Address'
                mysubnet.subnet_mask = [255,255,255,0]
            } else {
                mysubnet.class = 'Invalid Address'
            }
            
            return mysubnet
        },
        initSlider() {
            const input = $('#maskbit')
            const span = $('.maskbit')
        },
        setCurrentPreview(bit) {
            const obj = subnet.methods.lookupSubnetMask(bit);
            

            $('.preview-class').text(obj.class);
            $('#maskbit').val(obj.bit_mask);
            $('.maskbit').text(obj.bit_mask)
            $('.subnet-mask').text(obj.subnet_mask.join('.'));

            subnet.data.subnetmask = obj.subnet_mask;
            subnet.data.maskbit = obj.bit_mask;
        },
        setPreviewZone() {
            $('.ip-address').text(subnet.data.subnet.join('.'));
            $('.host-range').val(`${subnet.data.hostip.join('.')} to ${subnet.data.subnetmask.join('.')}`)
        },
        calculateSubnetMask(bit) {
            let arr = []
            let tmp_bit = parseInt(bit);
            for(let i = 0 ; i < 4 ; i++) {
                if(tmp_bit === 0) {
                    arr[i] = 0;
                } else if(tmp_bit - 8 >= 0) {
                    tmp_bit -= 8;
                    arr[i] = 255
                } else {
                    let tmp = '';
                    for (let j = 0; j < tmp_bit; j++) {
                        tmp += ''+1; 
                    }
                    let binary = tmp.padEnd(8,0);
                    arr[i] = parseInt(binary,2);
                    tmp_bit = 0;
                }
            }
            
            return arr;
        },
        setCalculatePreview() {
            const host = $('.host-1');
            const mask = $('.mask-1');
            const submask = $('.submask-1');
            const bc = $('.bc-1');

            host.each((i, el) => {
                $(el).closest('div').find('label').text(subnet.data.hostip[i])
                $(el).val(subnet.methods.numToBinary(subnet.data.hostip[i], 'start'))
            })

            mask.each((i, el) => {
                $(el).closest('div').find('label').text(subnet.data.subnetmask[i])
                $(el).val(subnet.methods.numToBinary(subnet.data.subnetmask[i]))
            })

            submask.each((i, el) => {
                $(el).closest('div').find('label').text(subnet.data.subnetmask[i] & subnet.data.hostip[i])
                subnet.data.subnet[i] = subnet.data.subnetmask[i] & subnet.data.hostip[i];
                let myhost = subnet.methods.numToBinary(subnet.data.subnetmask[i] & subnet.data.hostip[i])
                $(el).val(myhost)
            })

            let tmp = []
            tmp = subnet.data.hostip.map((e) => subnet.methods.numToBinary(e))
            let str = tmp.join('');
            
            let tmp2 = str.substring(0, subnet.data.maskbit);
            for (var j = parseInt(subnet.data.maskbit)+1 ; j <= str.length; j++) {
                tmp2 += '1'
            }

            str = ''; tmp = [];
            for(let i = 0 ; i <= tmp2.length; i++) {
                if(i % 8 != 0) {
                    str += tmp2[i];
                } else {
                    tmp.push(str);
                    str = tmp2[i];
                }
            }
            tmp.shift();
            subnet.data.boardcast = []
            bc.each((i, el) => {
                subnet.data.boardcast.push(parseInt(tmp[i],2))
                $(el).closest('div').find('label').text(parseInt(tmp[i],2))
                $(el).val(tmp[i])
            })
            $('.boardcast').text(subnet.data.boardcast.join('.'))
        },
        replaceAt(str, index, replacement) {
            return str.substring(0, index) + replacement + str.substring(index + replacement.length);
        }
    },
    init() {
        // Host IP Input Form
        $('.hostid').on('keyup', (e) => {
            const order = $(e.target).data('order');

            if(order === 1) {
                this.methods.setCurrentPreview(e.target.value);
            }

            if(e.target.value.length === 3) {
                if(order + 1 != 5) {
                    $(`input[data-order="${order+1}"]`).focus();
                }
            }
            if(e.target.value.length > 3) {
                e.target.value = e.target.value.substring(0,3);
            } 
            if(e.key === 'Backspace' && e.target.value.length === 0) {
                if(order - 1 != 0) {
                    $(`input[data-order="${order-1}"]`).focus();
                }
            }
            if(e.target.value > 255) {
                e.target.value = 255
            }
            // Set Value
            this.data.hostip[order-1] = e.target.value || 0
            this.methods.setCalculatePreview()
            this.methods.setPreviewZone()
            // this.methods.calculateSubnetMask()
        })

        // Maskbit with input range
        $('#maskbit').on('input', (e) => {
            $('.maskbit').text(e.target.value)
            this.data.maskbit = e.target.value

            this.data.subnetmask = subnet.methods.calculateSubnetMask(e.target.value);
            $('.subnet-mask').text(this.data.subnetmask.join('.'));
            this.methods.setCalculatePreview()
            this.methods.setPreviewZone()
            // subnet.methods.calculateSubnetMask(bit);
        })
    }
}

subnet.init();

String.prototype.replaceAt = function(index, replacement) {
    
}